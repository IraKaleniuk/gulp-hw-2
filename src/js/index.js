const navbar = document.querySelector(".navbar");
const burgerBtn = document.querySelector(".header__burger");
const burgerSpanArr = document.querySelectorAll(".header__burger-span");

burgerBtn.addEventListener("click", (e) => {
    for(let span of burgerSpanArr) {
        span.classList.toggle("header__burger-span--active");
    }
    navbar.classList.toggle("navbar--active");
})